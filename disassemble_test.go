package parter

import (
	"fmt"
	"os"
	"path"
	"testing"
)

func TestBreak(t *testing.T) {
	count, filePath := int64(2), path.Join("testdata", "chunk1.test")

	// Try to chunk a file that doesn't exists
	if err := Break("DOES_NOT_EXIST", 10); err == nil {
		t.Error("Expected an error since file does not exist")
	} //if

	// Get the file info
	info, err := os.Stat(filePath)

	if err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// Store the size
	size := info.Size()

	// Check for an error trying to create more files than bytes
	if err := Break(filePath, size+1); err == nil {
		t.Error("Expected error for too many files requested")
	} //if

	// Attempt to break into 2 files
	if err := Break(filePath, 2); err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	expectedSize := size / count

	// Check the size of the first file
	if info, err := os.Stat(filePath + ".part1"); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if info.Size() != expectedSize {
		t.Error("Expected the size of break2.test.part1 to be", expectedSize,
			"but was", info.Size())
	} //if

	// Check the size of the second file
	if info, err := os.Stat(filePath + ".part1"); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if info.Size() != expectedSize {
		t.Error("Expected the size of break2.test.part2 to be", expectedSize,
			"but was", info.Size())
	} //if

	// Remove the files
	for i := int64(1); i <= count; i++ {
		// Remove a part file
		os.Remove(fmt.Sprintf("%s.part%d", filePath, i))
	} //for
} //TestBreak

func TestChunk(t *testing.T) {
	// Some values
	chunkSize, testFilePath := int64(1024), path.Join("testdata", "break1.test")

	// Try to chunk a file that doesn't exists
	if err := Chunk("DOES_NOT_EXIST", 10); err == nil {
		t.Error("Expected an error since file does not exist")
	} //if

	// Try to chunk a file
	if err := Chunk(testFilePath, chunkSize); err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// Get the size of the base file
	info, err := os.Stat(testFilePath)

	// Check the error
	if err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// Calcualte the number of files
	numFiles := info.Size() / chunkSize

	// Check the number of files
	for i := int64(1); i <= numFiles; i++ {
		// Create the part name
		part := fmt.Sprintf("%s.part%d", testFilePath, i)

		// Check if the file exists
		if _, err := os.Stat(part); err != nil {
			t.Error("Expected file", part, "to exist")
		} else {
			// Remove the file
			os.Remove(part)
		} //if
	} //for
} //TestChunk
