package parter

import "testing"

func TestToSize(t *testing.T) {
	// Some bad values
	badValues := []string{"", "a", "10", "F"}

	// Some good values
	sizeB, sizeK, sizeM, sizeG := "10B", "10K", "10M", "10G"

	// Loop trough all the bad string
	for _, bad := range badValues {
		// Make sure the current string creates an error
		if _, err := ToSize(bad); err == nil {
			t.Error("Expected an err or for ", bad)
		} //if
	} //for

	// Check a the Byte size
	if size, err := ToSize(sizeB); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if expected := int64(10); size != expected {
		t.Error("Expected size to be", expected, "but was", size)
	} //if

	// Check a the Kilobyte size
	if size, err := ToSize(sizeK); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if expected := 10 * kilo; size != expected {
		t.Error("Expected size to be", expected, "but was", size)
	} //if

	// Check a the Megabyte size
	if size, err := ToSize(sizeM); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if expected := 10 * mega; size != expected {
		t.Error("Expected size to be", expected, "but was", size)
	} //if

	// Check a the Gigabyte size
	if size, err := ToSize(sizeG); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if expected := 10 * giga; size != expected {
		t.Error("Expected size to be", expected, "but was", size)
	} //if
} //TestToSize
