package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/antizealot1337/parter"
)

const (
	version = "Parter V1.1"
	usage   = version + `
Usage:
  parter (-delete) assemble|a [file]
  parter (-delete) disassemble|d <file> <size | count>
  parter usage|help
  parter version

Commands:
  assemble, a     Recreate a file from the parts
  disassemble, d  Breaks a file into parts
  usage, help     Shows help info
  version         Prints the version

Options:
	-delete  Deletes the original file(s) after reassembly/disassembly.
  file     The file to break apart or put back together
  size     The size of the files to break apart
  count    The number of files into which the starting file will be broken.
`
)

var (
	cmd       string
	filename  string
	sizeCount string
	deleteOld bool
	help      bool
)

func main() {
	// Indicates the program should delete the old data (file or parts) on success
	flag.BoolVar(&deleteOld, "delete", deleteOld, "Provides")

	// These private "-help", and "-usage" from showing the flag usage
	flag.BoolVar(&help, "help", help, "")
	flag.BoolVar(&help, "usage", help, "")

	// Parse the command line flags
	flag.Parse()

	// The number of arguments
	numArgs := flag.NArg()

	// Check if just the command was run
	if numArgs == 0 {
		fmt.Print(usage)
		os.Exit(0)
	} //if

	// Get the command
	cmd = flag.Arg(0)

	// Check if the file(name) was provided
	if numArgs > 1 {
		filename = flag.Arg(1)
	} //if

	// Check if the size/count was provided
	if numArgs > 2 {
		sizeCount = flag.Arg(2)
	} //if

	// Check for the commands
	switch cmd {
	case "assemble", "a":
		// Check if file name is empty
		if filename == "" {
			// Get the current directory
			wd, err := os.Getwd()

			// Check for an error
			if err != nil {
				exitErr(-1, err)
			} //if

			// Attempt to guess the original file name
			if guess, err := parter.Guess(wd); err != nil {
				exitErr(-1, err)
			} else if guess == "" {
				// Inform the user that no filename was provided or guessed and exit
				exitErrMsg(-1, "No original filename provided and unable to guess.")
			} else {
				filename = guess
			} //if
		} //if

		fmt.Println("Assembling", filename)

		// Assemble the file parts into the original
		if err := parter.Assemble(filename); err != nil {
			exitErr(-1, err)
		} //if

		// Check if we should delete the part files
		if deleteOld {
			fmt.Println("Deleting old files...")

			for i := 1; ; i++ {
				// Generate the part file name
				name := fmt.Sprintf("%s.part%d", filename, i)

				// Attempt to remove the file
				if err := os.Remove(name); err != nil {
					break
				} //if
			} //for

			fmt.Println("Done!")
		} //if
	case "disassemble", "d":
		// Make sure the file(name) and size were provided
		ensureNotEmpty(filename, "Error: file not provided")

		// Make sure the size or count was provided
		ensureNotEmpty(sizeCount, "Error: size or file count not provided")

		// Check if the user provided a size or a count
		if count, err := strconv.ParseInt(sizeCount, 10, 64); err == nil {
			fmt.Printf("Breaking %s into %d files\n", filename, count)

			// Break the file
			if err := parter.Break(filename, count); err != nil {
				exitErr(-1, err)
			} //if
		} else if size, err := parter.ToSize(sizeCount); err == nil {
			fmt.Printf("Breaking %s into files of size %d bytes\n", filename, size)

			// Chunk the file
			if err := parter.Chunk(filename, size); err != nil {
				exitErr(-1, err)
			} //if
		} else {
			exitErr(-1, err)
		} //if

		// Check if the original file should be deleted
		if deleteOld {
			fmt.Printf("Deleting %s...\n", filename)

			// Delete and check for an error
			if err := os.Remove(filename); err != nil {
				exitErr(-1, err)
			} //if
		} //if

		fmt.Println("Done!")
	case "usage", "help":
		fmt.Print(usage)
		os.Exit(0)
	case "version":
		fmt.Println(version)
		os.Exit(0)
	case "":
		fmt.Print(usage)
		os.Exit(0)
	default:
		fmt.Printf("%s is not a valid command\n", cmd)
		fmt.Print(usage)
		os.Exit(0)
	} //switch
} //main

func exitErr(code int, err error) {
	exitErrMsg(code, err.Error())
} //exitErr

func exitErrMsg(code int, message string) {
	// Print the message
	fmt.Fprintln(os.Stderr, message)

	// Exit
	os.Exit(code)
} //exitErrMsg

func ensureNotEmpty(value, message string) {
	// Check if the value was empty
	if value == "" {
		fmt.Println(message)
		fmt.Print(usage)
		os.Exit(-1)
	} //if
} //ensureNotEmpty
