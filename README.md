# Parter[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/parter?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/parter)

Parter is a command line utility (and library) for breaking a file into smaller
pieces and reassembling it. While meant to be primarily a command line utility,
there a a few methods exported for library use. Check out the link to the
https://godoc.org documentation above.

## Why?
Because.

## Usage
The application provides all the available options by running ```parter usage```
so examples will be given instead of a full explanation of the command line
interface.

To break a file name test.txt into chunks of 10MB size use:
```
$ parter d test.txt 10M # Application will probably complain unless you have a
10M+ text file lying around
```

**NOTE:** The application accepts B, M, K, and G (why?) for sizes and
understands them to be bytes, kilobytes, megabytes, and gigabytes (again why?)
respectively. Without a size indicator the program will assume the number is
a file count as shown below.

To break a file name test.txt into 2 files use:
```
$ parter d test.txt 2
```

To reassemble the file use:
```
$ parter a test.txt
```

## TODO
* ~~Remove need to specify the original file name for reassembling.~~
* ~~Add flag to remove original file or part files depending on operation.~~

## License
All Go source files are copyrighted and licensed under the terms of the MIT
License. View LICENSE file for more information.
