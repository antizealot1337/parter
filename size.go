package parter

import (
	"errors"
	"strconv"
)

const (
	kilo int64 = 1024
	mega int64 = kilo * 1024
	giga int64 = mega * 1024
)

// ToSize converts a data volumn (i.e. 10K) string to an integer amount.
func ToSize(size string) (int64, error) {
	// Make sure the string has a length greater than zero
	if len(size) == 0 {
		return 0, errors.New("size parse error: attempted to parse empty string")
	} //if

	// The last of the string
	size, last := size[:len(size)-1], size[len(size)-1:]

	// The size multiple
	var multiple int64

	// Make sure the suffix is an expected suffix
	switch last {
	case "B":
		multiple = 1
	case "K":
		multiple = kilo
	case "M":
		multiple = mega
	case "G":
		multiple = giga
	default:
		return 0, errors.New("size parse error: sizes need a B, K, M, or G suffix")
	} //case

	// Attempt to read the size
	ssize, err := strconv.ParseInt(size, 10, 64)

	// Check for an error
	if err != nil {
		return 0, err
	} //if

	// Calculate the size and return it
	return ssize * multiple, nil
} //ToSize
