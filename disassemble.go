package parter

import (
	"errors"
	"fmt"
	"io"
	"os"
)

// Break the file into a number of smaller files.
func Break(filename string, count int64) error {
	// Open the file for reading
	src, err := os.Open(filename)

	// Check the error
	if err != nil {
		return err
	} //if

	// Make sure the file is closed at exit
	defer src.Close()

	// Get the file info
	info, err := src.Stat()

	// Check the error
	if err != nil {
		return err
	} //if

	if count > info.Size() {
		return errors.New("break error: requested too many files")
	} //if

	// Calculate the size of the individial files and the count
	fileCount, size := int64(1), info.Size()/count

	// Start looping
	for {
		// Create the path for the part
		dstPath := fmt.Sprintf("%s.part%d", filename, fileCount)

		// Open the file
		dst, err := os.Create(dstPath)

		// Check for an error
		if err != nil {
			return err
		} //if

		// Make sure the file is closed
		defer dst.Close()

		// Determine how to write
		if fileCount != count {
			// Check for an error
			if _, err = io.CopyN(dst, src, size); err != nil {
				return err
			} //if
		} else {
			// Copy all the remaining code to the last file
			io.Copy(dst, src)

			// Exit the loop
			break
		} //if

		// Increment the fileCount
		fileCount++
	} //for

	return nil
} //Break

// Chunk the file into a number of smaller files with the provided files size.
func Chunk(filename string, size int64) error {
	// Open the file for reading
	src, err := os.Open(filename)

	// Check the error
	if err != nil {
		return err
	} //if

	// Make sure the source file is closed at exit
	defer src.Close()

	// The count of files
	count := 1

	// Loop until the end of the file
	for {
		// Create the path for the part
		dstPath := fmt.Sprintf("%s.part%d", filename, count)

		// Open a new file
		dst, err := os.Create(dstPath)

		// Check for an error
		if err != nil {
			return err
		} //if

		if count, err := io.CopyN(dst, src, size); err != nil {
			if count == 0 {
				// Close the file
				dst.Close()

				// Remove the file
				os.Remove(dstPath)
			} //if

			// If it is an EOF error
			if err.Error() == "EOF" {
				break
			} //if

			return err
		} //if

		// Increment count
		count++
	} //for

	return nil
} //Chunk
