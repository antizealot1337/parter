package parter

import (
	"path"
	"testing"
)

func TestGuess(t *testing.T) {
	// Some values
	dir := "testdata"

	// Make sure guessing in an invalid directory throws an error
	if _, err := Guess("DIR_DOES_NOT_EXIST"); err == nil {
		t.Error("Expected error since directory does not exist")
	} //if

	// Test a directory that is empty and cannot be guessed
	if guess, err := Guess(path.Join(dir, "guess2")); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if guess != "" {
		t.Errorf("Expected guess to be \"\" but was \"%s\"", guess)
	} //if

	// Test a directory that contains part files and can be guessed
	if guess, err := Guess(path.Join(dir, "guess1")); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if guess != "guess1" {
		t.Errorf("Expected the guessed name to be \"guess1\" but was \"%s\"",
			guess)
	} //if
} //TestGuess
