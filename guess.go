package parter

import (
	"os"
	"path"
	"strings"
)

// Guess the name of the original file based on the presence of part files.
// ".part1" in particular. If the original file cannot be guess an empty string
// is returned.
func Guess(directory string) (string, error) {
	// Open the directory
	dir, err := os.Open(directory)

	// Check for an error
	if err != nil {
		return "", err
	} //if

	// Make sure the directory is closed
	defer dir.Close()

	// Get the names of all the files in the directory
	names, err := dir.Readdirnames(-1)

	// Loop through the contents of the directory
	for _, name := range names {
		// Check if the file has a .part1 extension
		if path.Ext(name) == ".part1" {
			// Get the original name
			org := name[:strings.LastIndex(name, ".part1")]

			// Return this name without the .part1 extension
			return org, nil
		} //if
	} //for

	// Return an empty string and no error since no error occured but the file
	// name could not be guessed.
	return "", nil
} //Guess
