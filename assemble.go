package parter

import (
	"fmt"
	"io"
	"os"
)

// Assemble the original file from part files
func Assemble(original string) error {
	// Create the original file
	dst, err := os.Create(original)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Make sure the file is closed at exit
	defer dst.Close()

	// The count of the number of part files
	partCount := 1

	for {
		// The part file parth
		srcPath := fmt.Sprintf("%s.part%d", original, partCount)

		// Attempt to open a file
		src, err := os.Open(srcPath)

		// Check for an error
		if err != nil {
			// Check if this is the first file
			if partCount == 1 {
				// Delete the file
				defer os.Remove(original)

				// Return an error
				return fmt.Errorf("assemble error: no part files exist")
			} //if

			// Exit the loop
			break
		} //if

		// Make sure the file is closed at exit
		defer src.Close()

		// Copy from the part file
		if _, err := io.Copy(dst, src); err != nil {
			return err
		} //if

		// Increment the count
		partCount++
	} //for

	return nil
} //Assemble
