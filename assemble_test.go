package parter

import (
	"fmt"
	"os"
	"path"
	"testing"
)

func TestAssemble(t *testing.T) {
	// Some values
	filePath := path.Join("testdata", "assemble1.test")

	// Make sure an error is thrown if there is no <path>.part1 file
	if err := Assemble("DOES_NOT_EXIST"); err == nil {
		t.Error("Expected error due to no <path>.part1+ files")
	} //if

	// Attempt to assemble the test file
	if err := Assemble(filePath); err != nil {
		t.Error("Unexpected error:", err.Error())
	} //if

	// The expectedSize
	var expectedSize int64

	// Calculate the size of the part files
	for i := 1; i <= 4; i++ {
		// The part file's path
		fp := fmt.Sprintf("%s.part%d", filePath, i)

		// Get the info for the part file
		if info, err := os.Stat(fp); err != nil {
			t.Error("Unexpected error:", err.Error())
		} else {
			// Add to the size
			expectedSize += info.Size()
		} //if
	} //for

	// Check the full size
	if info, err := os.Stat(filePath); err != nil {
		t.Error("Unexpected error:", err.Error())
	} else if info.Size() != expectedSize {
		t.Error("Expected the file size to be", expectedSize, "but was",
			info.Size())
	} //if

	// Delete the assembled file
	os.Remove(filePath)
} //TestAssemble
